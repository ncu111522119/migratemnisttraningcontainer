# Migrate Mnist Traning
![](./public/image/Podman_criu.jpg)
# 目的
使用 Podman 與 CRIU 等技術，完成 CPU 的 Conatainer 狀態轉移，於另一台 host 從**中斷點啟動** Container。
使用的應用情境為 **MNIST Training** 階段的 Container 狀態恢復。

## keyword
`Container Migrate `、`Stateful Migrate`、`Mnist Training`、`CRIU`、`Checkpoint\Rrstore`

# 檔案架構
* [MnistTraining-Python](./MnistTraining-Python/)
  * [train_mnist.py](./MnistTraining-Python/train_mnist.py) : 執行 Mnist 的 Python Code
* [Dockerfile](./Dockerfile) : 將 MnistTraining-Python 打包為 Image

# 系統環境
* Virtualbox
  * OS: Ubuntu 22.04.2 LTS
  * hostname
    * worker2
    * worker3
* 安裝與設定
  * [Virtual Box 安裝](https://hackmd.io/2ksB7gXxRNqjN6KVxvyWqg)
  * [virtualbox 拷貝 VM](https://hackmd.io/vQLPXDsPRO6NrqWndBHpNQ)


# 所需套件
* [Podman](https://podman.io/) v4.5.1
  * [Building from scratch](https://podman.io/docs/installation#building-from-scratch): 安裝相依套件
  * [Get Source Code](https://podman.io/docs/installation#get-source-code): 安裝Podman
* [CRIU](https://criu.org/Installation) v3.18
  * Other stuff: 安裝相關套件
  * Obtaining CRIU sources: 複製連結，下載套件
  * [安裝教學](https://hackmd.io/ArY518rkTBaT731vzPh7Vw#criu)
* [CRUN](https://github.com/containers/crun) v1.8.5.0.0.0.44-017b
  * [Dependencies-ubuntu](https://github.com/containers/crun#ubuntu)
  * [Build](https://github.com/containers/crun#build)
  * [安裝教學](https://hackmd.io/ArY518rkTBaT731vzPh7Vw#Error-Runtime-Not-Support)

## 版本展示
```
renjie@worker2:~$ podman -v
 podman version 4.5.1

renjie@worker2:~$ criu -V
 Version: 3.18

renjie@worker2:~$ crun -v
 crun version 1.8.5.0.0.0.44-017b
 commit: 017bd29f5a99823c6c598fa875a41f316f16e58b
 rundir: /run/user/1000/crun
 spec: 1.0.0
 +SYSTEMD +SELINUX +APPARMOR +CAP +SECCOMP +EBPF +CRIU +YAJL
```



# Podman Contianer Stateful Migrate
## 執行指令
## podman checkpoint
```
$ sudo podman run -d --name looper busybox /bin/sh -c \
         'i=0; while true; do echo $i; i=$(expr $i + 1); sleep 1; done'
$ sudo podman ps

$ sudo podman logs <contianer ID>

$ sudo podman container checkpoint <contianer ID> --tcp-established=true --export=/tmp/chkpt_<contianer ID>.tar.gz

$ sudo scp /tmp/chkpt_<containerID>.tar.gz renjie@10.52.52.86:/tmp
```
## 執行 Podman restore
```
$ sudo podman container restore --tcp-established=true -i /tmp/chkpt_<containerID>.tar.gz
```

## 比對 log 接續 Print 狀態
### 在兩台機器上執行
```
$ sudo podman logs -f <containerID>
```


# Training a model in a Podman container with checkpoint
## At master 
### Pull Tensorflow Image，並選擇 Docker hub
```
$ sudo podman image pull tensorflow/tensorflow:latest-py3
? Please select an image:
    registry.fedoraproject.org/tensorflow/tensorflow:latest-py3
    registry.access.redhat.com/tensorflow/tensorflow:latest-py3
  ▸ docker.io/tensorflow/tensorflow:latest-py3
    quay.io/tensorflow/tensorflow:latest-py3

# 查看是否有該 Image
$ sudo podman image ls
```
### 將 MNIST Training 打包為Image
```
$ cd /home/renjie/lab/5_TraningDocker_checkpoint

$ sudo podman build . -t traning_mnist

# 查看是否有該 Image
$ sudo podman image ls
REPOSITORY                       TAG              IMAGE ID      CREATED        SIZE
localhost/traning_mnist          latest           a4f6f9f59f61  2 minutes ago  1.67 GB
```
### 執行 Podman Containe - MNIST Training
```
$ sudo podman run -d --name mnist_container localhost/traning_mnist

# -l : 代表最新的 Container，-f 會持續追蹤 Log
$ sudo podman logs -f -l

```
### 設定 Podman Container Checkpoint
```
$ sudo podman container checkpoint 1f020fe25afc -e /tmp/chkpt_1f020fe25afc.tar.gz
```
### 將 檔案複製於另一台 Host
#### 複製 Checkpoint
```
$ sudo scp /tmp/chkpt_1f020fe25afc.tar.gz renjie@10.52.52.87:/tmp

```
#### 複製 MNIST Image
```
# 輸出 MNIST Image
$ sudo podman save -o traning_mnist_master.tar localhost/traning_mnist

$ sudo scp ./traning_mnist_master.tar renjie@10.52.52.87:/home/renjie/lab
```

## At worker 
### Pull Tensorflow Image，並選擇 Docker hub
```
$ sudo docker image pull tensorflow/tensorflow:latest-py3
```
### 載入 MNIST Image
```
$ sudo podman load -i traning_mnist_master.tar
```
### Podman Container 從中斷點恢復
```
$ sudo podman container restore -i /tmp/chkpt_97d24604241d.tar.gz
```

## Error
```
$ sudo podman container restore -i /tmp/chkpt_97d24604241d.tar.gz
[sudo] password for renjie: 
Trying to pull localhost/traning_mnist:latest...
WARN[0004] Failed, retrying in 1s ... (1/3). Error: initializing source docker://localhost/traning_mnist:latest: pinging container registry localhost: Get "https://localhost/v2/": dial tcp 127.0.0.1:443: connect: connection refused 
WARN[0005] Failed, retrying in 1s ... (2/3). Error: initializing source docker://localhost/traning_mnist:latest: pinging container registry localhost: Get "https://localhost/v2/": dial tcp 127.0.0.1:443: connect: connection refused 
WARN[0006] Failed, retrying in 1s ... (3/3). Error: initializing source docker://localhost/traning_mnist:latest: pinging container registry localhost: Get "https://localhost/v2/": dial tcp 127.0.0.1:443: connect: connection refused 
Error: initializing source docker://localhost/traning_mnist:latest: pinging container registry localhost: Get "https://localhost/v2/": dial tcp 127.0.0.1:443: connect: connection refused
```
### Err: invalid repository name 
#### 問題描述
```
## At worker
$ sudo podman container restore -i /tmp/chkpt_97d24604241d.tar.gz 
Error: creating container storage: parsing named reference "a4f6f9f59f618396f4c49808098627dc810fbade1af32de34e50fd01235c6b22":
invalid repository name (a4f6f9f59f618396f4c49808098627dc810fbade1af32de34e50fd01235c6b22),
cannot specify 64-byte hexadecimal strings

## At master
# localhost/traning_mnist image 與上述的 ID 相同
$ sudo podman image ls
REPOSITORY                       TAG              IMAGE ID      CREATED         SIZE
localhost/traning_mnist          latest           a4f6f9f59f61  18 minutes ago  1.67 GB
```
#### 解決方法
將 相同的 Image 複製到 另一個 Host 中
```
## At master
$ sudo podman save -o traning_mnist_master.tar localhost/traning_mnist


$ sudo scp ./traning_mnist_master.tar renjie@10.52.52.87:/home/renjie/lab


## At worker 
$ sudo podman load -i traning_mnist_master.tar

$ sudo podman image ls
REPOSITORY                       TAG         IMAGE ID      CREATED         SIZE
localhost/traning_mnist          latest      a4f6f9f59f61  30 minutes ago  1.67 GB

```

* 參考資料: [podman案例：打包本地镜像，发送给别人或者上传镜像](https://blog.csdn.net/qq_45834685/article/details/121148116)


# Training a model in a Docker container with checkpoint

# 目的
試著運行一次Traning 並進行中斷點設置，查看是否從中斷點恢復後，能夠持續運行且模型是能夠無誤使用
## 可再嘗試
- Global value : 在API 請求計數
- DB : 在Container 之中使用 DB，不外掛 Volume

# 參考資料
* [使用 Docker 部署 TensorFlow 環境](https://tf.wiki/zh_hant/appendix/docker.html)
    * [](https://snippetinfo.net/mobile/media/2506) 
    * [docker hub - tensorflow/tensorflow](https://hub.docker.com/r/tensorflow/tensorflow/)

# 指令操作
## 建立 mnist 的 container
### 拉取 Tensorflow 的環境
```
sudo docker image pull tensorflow/tensorflow:latest-py3
sudo docker image ls
```

### 建立 mnist 的 docker image
```
git clone https://gitlab.com/ncu111522119/migratemnisttraningcontainer.git
cd migratemnisttraningcontainer
sudo docker build . -t training_mnist
sudo docker image ls
```

### 啟動 mnist image 的 container 
```
sudo docker run -d --name mnist_container training_mnist
sudo docker ps
```

#### 查看 Log
```
sudo docker logs -f <container id>

```

### 建立 check point
```
sudo docker checkpoint create mnist_container checkpoint_mnist
```
--leave-running
#### 查看 設定 checkpoint 的狀態
```
sudo docker ps -a
```
<!-- sudo docker start --checkpoint checkpoint_mnist mnist_container -->

## 實驗一: 移除 container

目的: 將停止的 Container 移除後，是否可單就 Checkpoint 啟動
### 移除 container
```
sudo docker rm <container id>
sudo docker ps -a #查看 Container 是否還存在
```
### Checkpoint 啟動 
```
sudo docker start --checkpoint checkpoint_mnist mnist_container
=> Error response from daemon: No such container: mnist_container
```

實驗結果: 無法單就Checkpoint 啟動 Container 