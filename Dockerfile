From tensorflow/tensorflow
WORKDIR /app

COPY ./MnistTraining_Python .

CMD ["python3", "train_mnist.py"]